import numpy as np
import pytest
from satis import io

initial_time = np.arange(0.5, 1.5, 0.01)
frequency = 4
initial_signal1 = 3 * np.sin(frequency * 2 * np.pi * initial_time) + 5
initial_signal2 = 6 * np.cos(frequency * 2 * np.pi * initial_time) + 5
multiple_signals = np.array([initial_signal1, initial_signal2])


def test_wrap_title():
    ref = "Some very long title\nthat needs to be\nshortened"
    out = io._wrap_title("Some very long title that needs to be shortened", 20)
    assert ref == out


def test_my_angle():
    ref = [0.0, np.pi / 2, np.pi]
    complex_array = [0.0 + 0j, 1j, -1.0 + 0j]
    out = io._my_angle(complex_array)


def test_display_header():
    io.display_header()


def test_extract_subset():
    initial_dataset = np.array([[0, 1, 2], [1, 2, 3], [2, 3, 4]])
    ref = [[0, 1, 2], [1, 2, 3]]
    out = io.extract_subset((0, 1), initial_dataset)
    np.testing.assert_allclose(ref, out)


# def test_read_param():


def test_read_signals(tmpdir):
    f = tmpdir.mkdir("sub").join("signal_file.dat")
    signal = "Time probe1 probe2\n"
    signal += "0 0 8\n"
    signal += "1 1 9\n"
    signal += "2 2 10\n"
    signal += "3 3 11"
    f.write(signal)
    ref_time = [0, 1, 2, 3]
    ref_signal = [[0, 1, 2, 3], [8, 9, 10, 11]]
    out_time, out_signal = io.read_signals(f)
    np.testing.assert_allclose(ref_time, out_time)
    np.testing.assert_allclose(ref_signal, out_signal)


# def test_read_signal_yaml():


def test_clean_signals():
    ref_time = np.arange(1.0, 1.5, 0.01)
    ref_signal = 3 * np.sin(4 * 2 * np.pi * ref_time) + 5
    new_time, new_signal = io.clean_signals(
        initial_time, initial_signal1, frequency, 0.8
    )
    np.testing.assert_allclose(new_time, ref_time)
    np.testing.assert_allclose(new_signal, ref_signal)


def test_set_default_target_freq():
    dt_ = initial_time[1] - initial_time[0]
    deltaf = 1 / dt_
    nyquist_freq = 0.5 * deltaf
    ref_freq = 0.3 * nyquist_freq
    out = io.set_default_target_freq(initial_time)
    assert ref_freq == out


def test_plot_time_signals():
    io.plot_time_signals(initial_time, multiple_signals, frequency, 0.8, "Pa")


@pytest.mark.skip(reason="no way of currently testing this")
def test_plot_fourier_convergence():
    io.plot_fourier_convergence(initial_time, multiple_signals, frequency, 0.8, "Pa")


@pytest.mark.skip(reason="no way of currently testing this")
def test_plot_psd_convergence():
    io.plot_psd_convergence(
        initial_time,
        multiple_signals,
        frequency,
        0.8,
        False,
        False,
        "Pa",
        1,
        "flat",
        0.0,
    )


def test_plot_fourier_variability():
    io.plot_fourier_variability(initial_time, multiple_signals, frequency, 0.8, "Pa")


@pytest.mark.skip(reason="no way of currently testing this")
def test_plot_psd_variability():
    io.plot_psd_variability(initial_time, multiple_signals, frequency, 0.8, "Pa")
