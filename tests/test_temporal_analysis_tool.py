from satis import temporal_analysis_tool as tat
import numpy as np
import matplotlib.pyplot as plt

# from matplotlib.testing.decorators import image_comparison
import pytest


# @image_comparison(baseline_images=['line_dashes'], remove_text=True,
#                   extensions=['png'])

initial_time_vector = np.arange(10, 10 + 2 * np.pi, 0.1)
initial_signal_vector = np.sin(initial_time_vector)
new_time, new_signal = tat.resample_signal(
    initial_time_vector, initial_signal_vector, 0.05
)


def test_resample_signal():

    check_value = [
        0.0,
        0.05,
        0.1,
        0.15,
        0.2,
        0.25,
        0.3,
        0.35,
        0.4,
        0.45,
        0.5,
        0.55,
        0.6,
        0.65,
        0.7,
        0.75,
        0.8,
        0.85,
        0.9,
        0.95,
        1.0,
        1.05,
        1.1,
        1.15,
        1.2,
        1.25,
        1.3,
        1.35,
        1.4,
        1.45,
        1.5,
        1.55,
        1.6,
        1.65,
        1.7,
        1.75,
        1.8,
        1.85,
        1.9,
        1.95,
        2.0,
        2.05,
        2.1,
        2.15,
        2.2,
        2.25,
        2.3,
        2.35,
        2.4,
        2.45,
        2.5,
        2.55,
        2.6,
        2.65,
        2.7,
        2.75,
        2.8,
        2.85,
        2.9,
        2.95,
        3.0,
        3.05,
        3.1,
        3.15,
        3.2,
        3.25,
        3.3,
        3.35,
        3.4,
        3.45,
        3.5,
        3.55,
        3.6,
        3.65,
        3.7,
        3.75,
        3.8,
        3.85,
        3.9,
        3.95,
        4.0,
        4.05,
        4.1,
        4.15,
        4.2,
        4.25,
        4.3,
        4.35,
        4.4,
        4.45,
        4.5,
        4.55,
        4.6,
        4.65,
        4.7,
        4.75,
        4.8,
        4.85,
        4.9,
        4.95,
        5.0,
        5.05,
        5.1,
        5.15,
        5.2,
        5.25,
        5.3,
        5.35,
        5.4,
        5.45,
        5.5,
        5.55,
        5.6,
        5.65,
        5.7,
        5.75,
        5.8,
        5.85,
        5.9,
        5.95,
        6.0,
        6.05,
        6.1,
        6.15,
    ]
    np.testing.assert_allclose(new_time, check_value, rtol=1e-5)

    check_value = [
        -0.54402111,
        -0.58454588,
        -0.62507065,
        -0.66247267,
        -0.69987469,
        -0.73378025,
        -0.76768581,
        -0.79775614,
        -0.82782647,
        -0.85376111,
        -0.87969576,
        -0.90123559,
        -0.92277542,
        -0.93970522,
        -0.95663502,
        -0.96878562,
        -0.98093623,
        -0.98818624,
        -0.99543625,
        -0.99771323,
        -0.99999021,
        -0.9972714,
        -0.99455259,
        -0.98686516,
        -0.97917773,
        -0.96659849,
        -0.95401925,
        -0.93667389,
        -0.91932853,
        -0.89739035,
        -0.87545217,
        -0.84914038,
        -0.82282859,
        -0.79240609,
        -0.76198358,
        -0.72775433,
        -0.69352508,
        -0.6558311,
        -0.61813711,
        -0.57735502,
        -0.53657292,
        -0.49311019,
        -0.44964746,
        -0.40393837,
        -0.35822928,
        -0.31073054,
        -0.26323179,
        -0.21441798,
        -0.16560418,
        -0.11596304,
        -0.0663219,
        -0.01634943,
        0.03362305,
        0.08342754,
        0.13323204,
        0.18237093,
        0.23150983,
        0.27949213,
        0.32747444,
        0.37382074,
        0.42016704,
        0.46441425,
        0.50866146,
        0.55036749,
        0.59207351,
        0.63082164,
        0.66956976,
        0.70497283,
        0.74037589,
        0.77208016,
        0.80378443,
        0.83147312,
        0.85916181,
        0.88255828,
        0.90595474,
        0.92482521,
        0.94369567,
        0.95785159,
        0.9720075,
        0.98130743,
        0.99060736,
        0.99495837,
        0.99930939,
        0.99866802,
        0.99802665,
        0.99239931,
        0.98677196,
        0.97621487,
        0.96565778,
        0.95027642,
        0.93489506,
        0.91484311,
        0.89479117,
        0.870269,
        0.84574683,
        0.81699945,
        0.78825207,
        0.75556671,
        0.72288135,
        0.68658459,
        0.65028784,
        0.61074235,
        0.57119687,
        0.52879778,
        0.48639869,
        0.44156963,
        0.39674057,
        0.34992946,
        0.30311836,
        0.25479292,
        0.20646748,
        0.15711057,
        0.10775365,
        0.05785842,
        0.00796318,
        -0.04197183,
        -0.09190685,
        -0.14138272,
        -0.19085858,
        -0.23938095,
        -0.28790332,
        -0.33498737,
        -0.38207142,
        -0.4272467,
    ]
    np.testing.assert_allclose(new_signal, check_value, rtol=1e-5)


def test_calc_autocorrelation_time():

    autocorrelation_time = tat.calc_autocorrelation_time(new_time, new_signal, 0.2)
    check_value = 1.416207599279301
    np.testing.assert_allclose(autocorrelation_time, check_value, rtol=1e-5)


def test_show_autocorrelation_time():

    fig = tat.show_autocorrelation_time(new_time, new_signal)


def test_sort_spectral_power():

    h_power, t_power = tat.sort_spectral_power(new_time, new_signal)
    check_value = [28.26561661001374, 623.5368313867725]
    np.testing.assert_allclose([h_power, t_power], check_value, rtol=1e-5)


def test_power_representative_frequency():

    frequency = tat.power_representative_frequency(new_time, new_signal, 0.8)
    check_value = 0.16260162601626016
    np.testing.assert_allclose(frequency, check_value, rtol=1e-5)


def test_show_power_representative_frequency():

    fig = tat.show_power_representative_frequency(new_time, new_signal)


def test_ks_test_distrib():
    np.random.seed(1)
    initial_array = np.array(
        [
            1.00818026,
            2.03522927,
            1.67906511,
            1.71086741,
            7.45442017,
            0.98271906,
            1.56969063,
            0.10373937,
            1.55657151,
            0.3094615,
            18.0145476,
            3.45091146,
            2.81358087,
            0.56983827,
            0.50487518,
            0.44476011,
            0.79313509,
            0.52103739,
            3.78903428,
            0.85649425,
            1.61033395,
            0.28465493,
            5.06404012,
            4.14755807,
            0.73240542,
        ]
    )
    score, position, height, scale = tat.ks_test_distrib(initial_array, "lognormal")
    assert score == pytest.approx(0.9948556484640008, 0.01)
    assert position == 0
    assert height is None
    assert scale == pytest.approx(1.300820573333199, 0.01)


def test_show_temperature_distribution():

    np.random.seed(1)
    initial_array = np.random.random((5, 5))
    height = np.linspace(0, 1, 5)
    fig = tat.show_temperature_distribution(initial_array, height)


def test_duration_for_uncertainty():

    duration = tat.duration_for_uncertainty(new_time, new_signal, 0.2)
    check_value = 273.3280666609051
    np.testing.assert_allclose(duration, check_value, rtol=1e-5)


def test_calculate_std():

    std = tat.calculate_std(new_time, new_signal, 30)
    check_value = 0.7101131020592177
    np.testing.assert_allclose(std, check_value, rtol=1e-5)


def test_power_spectral_density():

    freq, psdensity = tat.power_spectral_density(new_time, new_signal)
    print(psdensity)
    check_value = [
        2.42333242e-33,
        6.23233970e01,
        1.55594342e-02,
        4.78754477e-03,
        2.39738856e-03,
        1.45818340e-03,
        9.86717019e-04,
        7.14767595e-04,
        5.43044503e-04,
        4.27439148e-04,
        3.45795396e-04,
        2.85947673e-04,
        2.40747452e-04,
        2.05763313e-04,
        1.78126312e-04,
        1.55911027e-04,
        1.37785463e-04,
        1.22803932e-04,
        1.10279765e-04,
        9.97044804e-05,
        9.06949633e-05,
        8.29580757e-05,
        7.62664115e-05,
        7.04413544e-05,
        6.53410254e-05,
        6.08515705e-05,
        5.68807691e-05,
        5.33532831e-05,
        5.02070809e-05,
        4.73907164e-05,
        4.48612365e-05,
        4.25825564e-05,
        4.05241865e-05,
        3.86602280e-05,
        3.69685717e-05,
        3.54302576e-05,
        3.40289559e-05,
        3.27505475e-05,
        3.15827796e-05,
        3.05149833e-05,
        2.95378410e-05,
        2.86431923e-05,
        2.78238728e-05,
        2.70735791e-05,
        2.63867557e-05,
        2.57584991e-05,
        2.51844773e-05,
        2.46608621e-05,
        2.41842713e-05,
        2.37517213e-05,
        2.33605875e-05,
        2.30085734e-05,
        2.26936904e-05,
        2.24142487e-05,
        2.21688723e-05,
        2.19565570e-05,
        2.17768352e-05,
        2.16302417e-05,
        2.15198261e-05,
        2.14575932e-05,
        2.15120063e-05,
    ]
    np.testing.assert_allclose(psdensity, check_value, atol=1e-5)


def test_uncertainty_from_duration():
    target_value = 5.543615297398711
    value = tat.uncertainty_from_duration(
        0.05, 20, 10, confidence=0.95, distribution="normal"
    )
    assert target_value == value


def test_convergence_cartography():
    tat.convergence_cartography(initial_time_vector, initial_signal_vector)
