import os
import pytest
import numpy as np
import scipy.interpolate as interp
import matplotlib.mlab as mlab

from satis import satis_library as sali

initial_time = np.arange(0.5, 1.5, 0.01)

frequency = 4
initial_signal1 = 3 * np.sin(frequency * 2 * np.pi * initial_time) + 5
initial_signal2 = 6 * np.cos(frequency * 2 * np.pi * initial_time) + 5
multiple_signals = np.array([initial_signal1, initial_signal2])
ref_fft = (
    2
    * np.fft.rfft(multiple_signals, n=len(initial_signal1), axis=-1)
    / len(initial_signal1)
)
ref_fft[:, 0] /= 2.0
ref_fft1 = ref_fft[1, :]
sampling_frequency = 1.0 / (initial_time[1] - initial_time[0])


def test_remove_transient_time():
    new_time = sali.remove_initial_transient(initial_time, 0.8)
    np.testing.assert_allclose(new_time, np.arange(0.8, 1.5, 0.01))


def test_remove_transient_time_after_last_time():
    with pytest.raises(ValueError):
        new_time = sali.remove_initial_transient(initial_time, 1.6)


def test_define_good_time_array():
    new_time = sali.define_good_time_array(initial_time, frequency, 0.8)
    np.testing.assert_allclose(new_time, np.arange(1.0, 1.5, 0.01))
    new_time = sali.define_good_time_array(initial_time, frequency, 0.8, True)
    np.testing.assert_allclose(new_time, np.arange(0.5, 1.0, 0.01))


def test_interpolate_signals():
    ref_time = np.arange(0.5, 1.49, 0.005)
    interpolate_function = interp.interp1d(initial_time, initial_signal1, axis=-1)
    ref_signal = interpolate_function(ref_time)
    new_signal = sali.interpolate_signals(initial_time, initial_signal1, ref_time)
    np.testing.assert_allclose(ref_signal, new_signal)


def test_check_input_dimensions():
    new_time = np.array([initial_time, initial_time])
    single_signal = initial_signal1
    long_signal = np.append(single_signal, 4.0)
    try:
        sali._check_input_dimensions(new_time, initial_signal1)
    except ValueError:
        pass
    new_time = initial_time
    try:
        sali._check_input_dimensions(initial_time, np.array([multiple_signals]))
    except ValueError:
        pass
    try:
        sali._check_input_dimensions(initial_time, long_signal)
    except ValueError:
        pass


def test_get_clean_signals():
    ref_time = np.arange(1.0, 1.5, 0.01)
    ref_signal = 3 * np.sin(4 * 2 * np.pi * ref_time) + 5
    new_time, new_signal = sali.get_clean_signals(
        initial_time, initial_signal1, frequency, 0.8
    )
    np.testing.assert_allclose(new_time, ref_time)
    np.testing.assert_allclose(new_signal, ref_signal)


def test_get_clean_fft():
    new_time, new_signal = sali.get_clean_signals(
        initial_time, initial_signal1, frequency
    )
    ref_freqs = np.fft.rfftfreq(len(new_time), new_time[1] - new_time[0])
    freqs, spectrum, target = sali.get_clean_fft(
        initial_time, initial_signal1, frequency
    )
    ref_fft1 = 2 * np.fft.rfft(new_signal, n=len(new_signal), axis=-1) / len(new_signal)
    ref_fft1[0] /= 2.0
    np.testing.assert_allclose(ref_fft1, spectrum)
    np.testing.assert_allclose(ref_freqs, freqs)
    assert target == frequency


def test_get_coeff_fourier():
    ref_target_coefs = ref_fft[:, 4]
    new_target_coefs = sali.get_coeff_fourier(initial_time, multiple_signals, frequency)
    np.testing.assert_allclose(ref_target_coefs, new_target_coefs)


def test_get_clean_average():
    abs_fft1 = np.abs(ref_fft1[0])
    clean_fft1 = sali.get_clean_average(initial_time, initial_signal1, frequency)
    np.testing.assert_allclose(abs_fft1, clean_fft1)
    abs_fft = np.abs(ref_fft[:, 0])
    clean_fft = sali.get_clean_average(initial_time, multiple_signals, frequency)
    np.testing.assert_allclose(abs_fft, clean_fft)


def test_remove_average():
    new_signal = sali.remove_average(initial_signal1)
    mean = np.mean(initial_signal1)
    np.testing.assert_allclose(initial_signal1 - mean, new_signal)


def test_get_clean_psd():
    block_size = int(len(initial_signal1))
    new_time, new_signal = sali.get_clean_signals(
        initial_time, initial_signal1, frequency
    )
    ref_psd, ref_freqs = mlab.psd(
        new_signal,
        Fs=sampling_frequency,
        NFFT=block_size,
        window=np.ones(len(new_signal)),
    )
    new_freqs, new_psd, new_target = sali.get_clean_psd(
        initial_time,
        initial_signal1,
        frequency,
        transient_time=0.0,
        nb_blocks=1,
        window_func="flat",
        overlap_pct=0.0,
    )
    np.testing.assert_allclose(ref_psd, new_psd)

    _, psd_spectrum, _ = sali.get_clean_psd(initial_time, multiple_signals, frequency)

    fft_frequencies, fft_spectrum, index_target = sali.get_clean_fft(
        initial_time, multiple_signals, frequency
    )

    frequency_resolution = fft_frequencies[1] - fft_frequencies[0]

    # Compute PSD with the correct normalization
    ref_psd_spectrum = np.abs(fft_spectrum) ** 2 / (2.0 * frequency_resolution)
    ref_psd_spectrum[:, 0] *= 2.0
    np.testing.assert_allclose(ref_psd_spectrum, psd_spectrum)
