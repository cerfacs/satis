import os
import pytest
import numpy as np

from satis import avbp2global_compute_ftf as a2g_ftf

time = np.arange(0.0, 1.0, 0.01)
heat_release = 3 * np.sin(2 * np.pi * 4 * time + 0.1) + 2
velocity = 9 * np.sin(2 * np.pi * 4 * time + 0.2) + 6
parameters = {"transient_time": 0, "target_frequency": 4.0}


def test_compute_ftf():
    gain, delay, phase = a2g_ftf.get_flame_transfer_function(
        time, heat_release, velocity, parameters, show=True
    )
