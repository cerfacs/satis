# Changelog

All notable changes to this project will be documented in this file.
The format is based on Keep a Changelog,
and this project adheres to Semantic Versioning.

## [0.0.5] 2022 / 02 / 21 

### Added

 - setup.cfg file
 - .pylintrc to make pylint and black work together

### Changed

 - setup.py file, __init__ for version
 - removed the build folder
 - Updated the CLI, setup.cfg to display the info of the CLI
 - Added the version in the CLI

### Fixed

  Side effect : compute_autocorr_time() was modifying input signal - Fixed.

### Deprecated
 [ - ]




## [0.0.4] 2021 / 10 / 27 

### Added
  - Tests 
### Changed
  - Autocorrelation time is computed with a 1D regression
  - Autocorrelation time support is normalize (time-time[0])
### Fixed
 [ - ]

### Deprecated
 [ - ]


## [0.0.3] 2021 / 05 / 27

### Added
  - Tests, mostly on module satis_library 
  - Temporal_analysis_tool_module
### Changed
  - Improvement of documentation
  - Satis module renamed as io
### Fixed
 [ - ]

### Deprecated
 [ - ]

## [0.0.2] 2020 / 12 / 03

### Added
 [ - ]

### Changed
  - Cleaned useless dependences (glob3, pyyaml)
  - Improved README.md
  - Changed add_credit fonction to show url link instead of cerfacs logo
### Fixed
 [ - ]

### Deprecated
 [ - ]



## [0.0.0] 2020 / 11 / 24

### Added
 [ - ]

### Changed
 [ - ] 
 
### Fixed
 [ - ] 

### Deprecated
 [ - ] 



