
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta charset="utf-8" />
    <title>Interpretation of the output plots &#8212; Spectral  documentation</title>
    <link rel="stylesheet" href="_static/alabaster.css" type="text/css" />
    <link rel="stylesheet" href="_static/pygments.css" type="text/css" />
    <script id="documentation_options" data-url_root="./" src="_static/documentation_options.js"></script>
    <script src="_static/jquery.js"></script>
    <script src="_static/underscore.js"></script>
    <script src="_static/doctools.js"></script>
    <script src="_static/language_data.js"></script>
    <link rel="index" title="Index" href="genindex.html" />
    <link rel="search" title="Search" href="search.html" />
    <link rel="prev" title="Introduction" href="spectral_doc.html" />
   
  <link rel="stylesheet" href="_static/custom.css" type="text/css" />
  
  
  <meta name="viewport" content="width=device-width, initial-scale=0.9, maximum-scale=0.9" />

  </head><body>
  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          

          <div class="body" role="main">
            
  <div class="section" id="interpretation-of-the-output-plots">
<h1>Interpretation of the output plots<a class="headerlink" href="#interpretation-of-the-output-plots" title="Permalink to this headline">¶</a></h1>
<p>All the examples shown below are performed with the signals in “Uref_probes.dat”. These signals come from the LES of a reactive combustion chamber featuring a thermoacoustic instability. They correspond to the velocity measured at several probes inside the injector.</p>
<div class="section" id="time-diagnostic">
<h2>Time diagnostic<a class="headerlink" href="#time-diagnostic" title="Permalink to this headline">¶</a></h2>
<div class="section" id="usage">
<h3>Usage<a class="headerlink" href="#usage" title="Permalink to this headline">¶</a></h3>
<p><strong>For an analysis at 560 Hz:</strong></p>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="o">&gt;</span> <span class="n">spectral3</span> <span class="n">time</span> <span class="n">Uref_probes</span><span class="o">.</span><span class="n">dat</span> <span class="o">-</span><span class="n">f</span> <span class="mi">560</span>
</pre></div>
</div>
</div>
<div class="section" id="description">
<h3>Description<a class="headerlink" href="#description" title="Permalink to this headline">¶</a></h3>
<p align="center">
    <img width="70%" src="images/image6.png" alt>
</p><p>The time diagnostic displays:</p>
<ul class="simple">
<li><p>The input signals as a function of time.</p></li>
<li><p>The ensemble-average of all input signals as a function of time.</p></li>
<li><p>A cumulative time-average, performed on an increasing number of periods (inverse of target frequency), on the ensemble-average signal.</p></li>
</ul>
<p>The vertical grid lines divide the signals into periods of the target frequency, which makes it easy to determine how many periods the signal contains (11 in this example).</p>
<p>In the top plot, the user can check if the general shape of the input signals corresponds to what is expected. For a pulsed or excited computation for example, all signals should feature oscillations at the excitation frequency. The user can also check if all signals are equivalent, which is an important assumption if the user wants to smooth the signal with an ensemble average.</p>
<p>In the middle plot, this ensemble average is displayed and in the present example, the averaging procedure removes a considerable portion of the parasite fluctuations. This is what is usually expected from the ensemble average.</p>
<p>The bottom plot features the cumulative average over an increasing number of periods, which provides an indication of whether a permanent regime is reached or not. In a permanent regime, this curve should be almost flat. If an initial transient regime is observed, it can be removed with the “-t” or “–tinit” option.</p>
<p>For example:</p>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="o">&gt;</span> <span class="n">spectral3</span> <span class="n">time</span> <span class="n">Uref_probes</span><span class="o">.</span><span class="n">dat</span> <span class="o">-</span><span class="n">f</span> <span class="mi">560</span> <span class="o">-</span><span class="n">t</span> <span class="mf">0.201</span>
</pre></div>
</div>
<p><img alt="time" src="_images/image8.png" /></p>
</div>
<div class="section" id="quick-check-list">
<h3>Quick check-list<a class="headerlink" href="#quick-check-list" title="Permalink to this headline">¶</a></h3>
<ul class="simple">
<li><p>Check that all input signals correspond to what you are expecting.</p></li>
<li><p>Check that all input signals are similar.</p></li>
<li><p>Check that ensemble averaging correctly smoothes out the signal without removing the fluctuations at the frequency of interest.</p></li>
<li><p>Check that the cumulative time-average is flat. Otherwise remove a portion of signal with “–tinit” option.</p></li>
</ul>
</div>
</div>
<div class="section" id="fourier-variability-diagnostic">
<h2>Fourier variability diagnostic<a class="headerlink" href="#fourier-variability-diagnostic" title="Permalink to this headline">¶</a></h2>
<div class="section" id="id1">
<h3>Usage<a class="headerlink" href="#id1" title="Permalink to this headline">¶</a></h3>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="o">&gt;</span> <span class="n">spectral3</span> <span class="n">fouriervariability</span> <span class="n">Uref_probes</span><span class="o">.</span><span class="n">dat</span> <span class="o">-</span><span class="n">f</span> <span class="mi">560</span>
</pre></div>
</div>
</div>
<div class="section" id="id2">
<h3>Description<a class="headerlink" href="#id2" title="Permalink to this headline">¶</a></h3>
<p><img alt="fourier_variability" src="_images/image5.png" /></p>
<p>The Fourier variability diagnostic is composed of four plots.</p>
<ul class="simple">
<li><p>Plot 1 (top-left) shows the time-average value at each probe (circles) and the fluctuation amplitude (i.e. fourier amplitude) at the target frequency (vertical bar). The time-average and fluctuation amplitude of the ensemble-average is also shown as a blue horizontal line and blue rectangle respectively.</p></li>
<li><p>Plot 2 (top-right) shows the Fourier coefficients at the target frequency in the complex space. The amplitude and phase of the average Fourier coefficient are also displayed as a red circle and green line respectively.</p></li>
<li><p>Plot 3 (bottom-left) and 4 (bottom-right) show separately the module and phase of the target Fourier coefficient at each probe.</p></li>
</ul>
<p>In these plots, the user can check the coherence of the input signals. Indeed, it is a common practice (especially in acoustics) to average several signals in order to reduce noise, but this is valid only if the input signals are acoustically equivalent (same amplitude of fluctuations, same phase, similar time-average).</p>
<p>Additionally, the user can check the level of the fluctuations compared to the average in the top-left plot.</p>
</div>
<div class="section" id="id3">
<h3>Quick check-list<a class="headerlink" href="#id3" title="Permalink to this headline">¶</a></h3>
<ul class="simple">
<li><p>Check that all input signals provide similar results in the four screens i.e.</p>
<ul>
<li><p>All points are close to the blue line in plot 1. The size of the vertical bars should be similar to the vertical size of the blue rectangle.</p></li>
<li><p>All points are close to the intersection between red and green lines in plot 2.</p></li>
<li><p>All points are close to the red line in plot 3.</p></li>
<li><p>All points are close to the green line in plot 4.</p></li>
</ul>
</li>
<li><p>If this is not the case, the user should remove the bad signals from the inputs.</p></li>
<li><p>To verify a small fluctuations assumption, also check in plot 1 that all bars are small compared to the height of the points.</p></li>
</ul>
</div>
</div>
<div class="section" id="fourier-convergence-diagnostic">
<h2>Fourier convergence diagnostic<a class="headerlink" href="#fourier-convergence-diagnostic" title="Permalink to this headline">¶</a></h2>
<p><strong>WARNING !</strong> This diagnostic works on the ensemble average (average signal).</p>
<div class="section" id="tool-usage">
<h3>Tool usage<a class="headerlink" href="#tool-usage" title="Permalink to this headline">¶</a></h3>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="o">&gt;</span> <span class="n">spectral3</span> <span class="n">fourierconvergence</span> <span class="n">Uref_probes</span><span class="o">.</span><span class="n">dat</span> <span class="o">-</span><span class="n">f</span> <span class="mi">560</span>
</pre></div>
</div>
</div>
<div class="section" id="id4">
<h3>Description<a class="headerlink" href="#id4" title="Permalink to this headline">¶</a></h3>
<p><img alt="fourier_convergence" src="_images/image2.png" /></p>
<p>Since this diagnostic is based on the average signal, the user should have checked beforehand that all input signals are equivalent thanks to the “fourier_variability” diagnostic.</p>
<p>The top plots show the amplitude of the Discrete Fourier Transform performed on the complete average signal, the last “half” of the signal and the last “quarter” of the signal. Note that just like the complete signal, the “half” and “quarter” signals are made of a whole number of periods. If the signal does not contain enough periods to define the first half and the first quarter, a warning message is displayed and the corresponding curve is not shown. The vertical dashed line indicates the position of the target frequency (0.3 times the Nyquist frequency if no target frequency was specified by the user).</p>
<p>The bottom two plots show the amplitude and phase of the Fourier Transform at the target frequency on an increasingly large time support.</p>
</div>
<div class="section" id="id5">
<h3>Quick check-list<a class="headerlink" href="#id5" title="Permalink to this headline">¶</a></h3>
<ul class="simple">
<li><p>In the top two plots, the yellow, orange and red curved should have similar levels at all the frequencies of interest if the signal is long enough.</p></li>
<li><p>In the bottom two plots, the blue curves should converge towards a stationary value.</p></li>
</ul>
</div>
</div>
<div class="section" id="psd-variability">
<h2>PSD variability<a class="headerlink" href="#psd-variability" title="Permalink to this headline">¶</a></h2>
<div class="section" id="id6">
<h3>Tool usage<a class="headerlink" href="#id6" title="Permalink to this headline">¶</a></h3>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="o">&gt;</span> <span class="n">spectral3</span> <span class="n">psdvariability</span> <span class="n">Uref_probes</span><span class="o">.</span><span class="n">dat</span> <span class="o">-</span><span class="n">f</span> <span class="mi">560</span>
</pre></div>
</div>
</div>
<div class="section" id="id7">
<h3>Description<a class="headerlink" href="#id7" title="Permalink to this headline">¶</a></h3>
<p><img alt="psd_variability" src="_images/image7.png" /></p>
<p>This diagnostic shows the distribution of the spectral energy of fluctuations on the target frequency, its first and second harmonic and the rest of the frequencies. <br/>
<strong>Note that this distribution is related to the fluctuations and that the time-average has been removed from the signal.</strong></p>
<p>In this diagnostic, you can check whether a frequency of interest is strongly present in the signals, or not. For a broadband noise signal, this diagnostic might not be very useful. It is rather designed for forced signals, or signals of resonant cases.</p>
</div>
<div class="section" id="id8">
<h3>Quick check-list<a class="headerlink" href="#id8" title="Permalink to this headline">¶</a></h3>
<ul class="simple">
<li><p>The PSD content at the target frequency should be important for all probes.</p></li>
</ul>
</div>
</div>
<div class="section" id="psd-convergence">
<h2>PSD convergence<a class="headerlink" href="#psd-convergence" title="Permalink to this headline">¶</a></h2>
<p><strong>WARNING !</strong> This diagnostic works on the ensemble average (average signal).</p>
<div class="section" id="id9">
<h3>Tool usage<a class="headerlink" href="#id9" title="Permalink to this headline">¶</a></h3>
<p>With standard units</p>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="o">&gt;</span> <span class="n">spectral3</span> <span class="n">psdconvergence</span> <span class="n">Uref_probes</span><span class="o">.</span><span class="n">dat</span> <span class="o">-</span><span class="n">f</span> <span class="mi">560</span>
</pre></div>
</div>
<p>With db SPL</p>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="o">&gt;</span> <span class="n">spectral3</span> <span class="n">psdconvergence</span> <span class="n">Uref_probes</span><span class="o">.</span><span class="n">dat</span> <span class="o">-</span><span class="n">f</span> <span class="mi">560</span> <span class="o">--</span><span class="n">dbspl</span>
</pre></div>
</div>
</div>
<div class="section" id="id10">
<h3>Description<a class="headerlink" href="#id10" title="Permalink to this headline">¶</a></h3>
<p>Without “dbspl” option</p>
<p><img alt="psd_convergence_without_dbspl" src="_images/image4.png" /></p>
<p>With “dbspl” option</p>
<p><img alt="psd_convergence_with_dbspl" src="_images/image1.png" /></p>
<p>Just as the Fourier convergence, the PSD convergence diagnostic shows the Power Spectral Density obtained on the complete signal, the last half and the last quarter. The left uses a standard linear scale while the right plot shows the same result with log scales. The location of the target frequency is indicated by the dashed vertical line.</p>
<p>An experimental option, called “–discardphase” makes it possible to analyze the frequency content of the spectral power, regardless of the coherence between the different input signals. Else said, all signals are assumed to be synchronized at all frequencies. Else said again, the phase of the Fourier coefficients is artificially set to 0 for all signals.</p>
<p>Here is an example, with “–discardphase –dbspl”, on the same input signals.</p>
<p><img alt="psd_convergence_with_discardphase_dbspl" src="_images/image3.png" /></p>
</div>
<div class="section" id="id11">
<h3>Quick check-list<a class="headerlink" href="#id11" title="Permalink to this headline">¶</a></h3>
<ul class="simple">
<li><p>Check that all PSD curves are similar.</p></li>
<li><p>Check that the Parseval equality is well-respected (this information is displayed in the standard output).</p></li>
<li><p>Check the levels of the PSD.</p></li>
</ul>
</div>
</div>
</div>


          </div>
          
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper">
<h1 class="logo"><a href="index.html">Spectral</a></h1>








<h3>Navigation</h3>
<ul class="current">
<li class="toctree-l1"><a class="reference internal" href="spectral_doc.html">Introduction</a></li>
<li class="toctree-l1"><a class="reference internal" href="spectral_doc.html#installation">Installation</a></li>
<li class="toctree-l1"><a class="reference internal" href="spectral_doc.html#tool-usage">Tool usage</a></li>
<li class="toctree-l1 current"><a class="current reference internal" href="#">Interpretation of the output plots</a><ul>
<li class="toctree-l2"><a class="reference internal" href="#time-diagnostic">Time diagnostic</a><ul>
<li class="toctree-l3"><a class="reference internal" href="#usage">Usage</a></li>
<li class="toctree-l3"><a class="reference internal" href="#description">Description</a></li>
<li class="toctree-l3"><a class="reference internal" href="#quick-check-list">Quick check-list</a></li>
</ul>
</li>
<li class="toctree-l2"><a class="reference internal" href="#fourier-variability-diagnostic">Fourier variability diagnostic</a><ul>
<li class="toctree-l3"><a class="reference internal" href="#id1">Usage</a></li>
<li class="toctree-l3"><a class="reference internal" href="#id2">Description</a></li>
<li class="toctree-l3"><a class="reference internal" href="#id3">Quick check-list</a></li>
</ul>
</li>
<li class="toctree-l2"><a class="reference internal" href="#fourier-convergence-diagnostic">Fourier convergence diagnostic</a><ul>
<li class="toctree-l3"><a class="reference internal" href="#tool-usage">Tool usage</a></li>
<li class="toctree-l3"><a class="reference internal" href="#id4">Description</a></li>
<li class="toctree-l3"><a class="reference internal" href="#id5">Quick check-list</a></li>
</ul>
</li>
<li class="toctree-l2"><a class="reference internal" href="#psd-variability">PSD variability</a><ul>
<li class="toctree-l3"><a class="reference internal" href="#id6">Tool usage</a></li>
<li class="toctree-l3"><a class="reference internal" href="#id7">Description</a></li>
<li class="toctree-l3"><a class="reference internal" href="#id8">Quick check-list</a></li>
</ul>
</li>
<li class="toctree-l2"><a class="reference internal" href="#psd-convergence">PSD convergence</a><ul>
<li class="toctree-l3"><a class="reference internal" href="#id9">Tool usage</a></li>
<li class="toctree-l3"><a class="reference internal" href="#id10">Description</a></li>
<li class="toctree-l3"><a class="reference internal" href="#id11">Quick check-list</a></li>
</ul>
</li>
</ul>
</li>
</ul>

<div class="relations">
<h3>Related Topics</h3>
<ul>
  <li><a href="index.html">Documentation overview</a><ul>
      <li>Previous: <a href="spectral_doc.html" title="previous chapter">Introduction</a></li>
  </ul></li>
</ul>
</div>
<div id="searchbox" style="display: none" role="search">
  <h3 id="searchlabel">Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="search.html" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" />
      <input type="submit" value="Go" />
    </form>
    </div>
</div>
<script>$('#searchbox').show(0);</script>








        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="footer">
      &copy;2020, Team COOP.
      
      |
      Powered by <a href="http://sphinx-doc.org/">Sphinx 2.4.4</a>
      &amp; <a href="https://github.com/bitprophet/alabaster">Alabaster 0.7.12</a>
      
      |
      <a href="_sources/usage_example.md.txt"
          rel="nofollow">Page source</a>
    </div>

    

    
  </body>
</html>