
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta charset="utf-8" />
    <title>Introduction &#8212; Spectral  documentation</title>
    <link rel="stylesheet" href="_static/alabaster.css" type="text/css" />
    <link rel="stylesheet" href="_static/pygments.css" type="text/css" />
    <script id="documentation_options" data-url_root="./" src="_static/documentation_options.js"></script>
    <script src="_static/jquery.js"></script>
    <script src="_static/underscore.js"></script>
    <script src="_static/doctools.js"></script>
    <script src="_static/language_data.js"></script>
    <link rel="index" title="Index" href="genindex.html" />
    <link rel="search" title="Search" href="search.html" />
    <link rel="next" title="Interpretation of the output plots" href="usage_example.html" />
    <link rel="prev" title="Welcome to Spectral’s documentation!" href="index.html" />
   
  <link rel="stylesheet" href="_static/custom.css" type="text/css" />
  
  
  <meta name="viewport" content="width=device-width, initial-scale=0.9, maximum-scale=0.9" />

  </head><body>
  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          

          <div class="body" role="main">
            
  <div class="section" id="introduction">
<h1>Introduction<a class="headerlink" href="#introduction" title="Permalink to this headline">¶</a></h1>
<p>This document introduces yet another tool for spectral analysis, called SPECTRAL. SPECTRAL is a python tool that relies on the numpy.fft library.</p>
<p>It is adapted to the analysis of CFD signals, which have the following characteristics:</p>
<ul class="simple">
<li><p><strong>Short sampling time.</strong> Small sampling times are not a problem in CFD and most frequencies of interest are correctly discretized in the time domain.</p></li>
<li><p><strong>Potentially short recording time.</strong> In Large Eddy Simulations of compressible flows, a recording time of a few seconds already requires an important amount of CPU times. For the study of processes with a long characteristic time, only a few dozens periods are usually recorded and it is important to check that the relevance of the spectral content of such short signals.</p></li>
<li><p><strong>Low signal-to-noise ratio.</strong> This is true for the study of acoustic resonances in turbulent flows. In this case, it is difficult to separate acoustic fluctuations from turbulent ones. One way to increase the acoustic content is to consider the average of several acoustically equivalent probes, but this assumption should be double checked.</p></li>
<li><p><strong>Multiple measures available.</strong> Contrary to experiments, measurements are very easy to perform anywhere in the domain. In acoustics, this is often used to increase the signal-to-noise ratio by averaging several signals at neighboring locations.</p></li>
</ul>
<p>This analysis is optimized for a single target frequency, chosen by the user. Special care is also taken to normalize the Fourier amplitudes correctly so that they are representative of the fluctuation amplitudes.</p>
</div>
<div class="section" id="installation">
<h1>Installation<a class="headerlink" href="#installation" title="Permalink to this headline">¶</a></h1>
<p>Spectral3 is available by simple execution of</p>
<blockquote>
<div><p>git clone git&#64;nitrox.cerfacs.fr:cfd-apps/spectral.git</p>
</div></blockquote>
<p>Then, go into Spectral3 directory to install Spectral3 :</p>
<blockquote>
<div><p>python setup.py install</p>
</div></blockquote>
</div>
<div class="section" id="tool-usage">
<h1>Tool usage<a class="headerlink" href="#tool-usage" title="Permalink to this headline">¶</a></h1>
<p>The tool can be called in command line. The option ‘-h’ or ‘–help’ shows how to use it.</p>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="o">&gt;</span> <span class="n">spectral3</span> <span class="o">--</span><span class="n">help</span>
</pre></div>
</div>
<p>Two mandatory arguments are expected:</p>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="o">&gt;</span> <span class="n">spectral3</span> <span class="n">diagnostic</span> <span class="n">filename</span>
</pre></div>
</div>
<p>With</p>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span>- diagnostic: the requested diagnostic among the list: “time”, “fourier-convergence”, “fourier-variability”, “psd-convergence”, “psd-variability”. Default is “time”.
- filename: the path to the text file containing the N signals to analyze. Default is “signals.dat”.
</pre></div>
</div>
<p>The tool can truncate the temporal signals so as to remove an initial portion of transient regime, and/or match the size of the signal so as to get the Fourier Transform at an exact frequency. In this case, the options -f and -t can be used.</p>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="o">&gt;</span> <span class="n">spectral3</span> <span class="n">filename</span> <span class="n">diagnostic</span> <span class="o">-</span><span class="n">f</span> <span class="n">frequency</span> <span class="o">-</span><span class="n">t</span> <span class="n">tinit</span>
</pre></div>
</div>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="o">-</span> <span class="n">frequency</span><span class="p">:</span> <span class="n">the</span> <span class="n">target</span> <span class="n">frequency</span> <span class="ow">in</span> <span class="n">Hz</span><span class="o">.</span> <span class="n">If</span> <span class="n">nothing</span> <span class="ow">is</span> <span class="n">specified</span><span class="p">,</span> <span class="mf">0.3</span> <span class="n">times</span> <span class="n">the</span> <span class="n">Nyquist</span> <span class="n">frequency</span> <span class="ow">is</span> <span class="n">chosen</span> <span class="n">instead</span><span class="o">.</span>
<span class="o">-</span> <span class="n">tinit</span><span class="p">:</span> <span class="n">the</span> <span class="n">initial</span> <span class="n">time</span> <span class="n">to</span> <span class="n">remove</span> <span class="kn">from</span> <span class="nn">the</span> <span class="n">signal</span> <span class="ow">in</span> <span class="n">seconds</span><span class="o">.</span> <span class="n">If</span> <span class="n">nothing</span> <span class="ow">is</span> <span class="n">specified</span><span class="p">,</span> <span class="n">the</span> <span class="n">complete</span> <span class="n">signals</span> <span class="n">are</span> <span class="n">used</span><span class="o">.</span>
</pre></div>
</div>
<p>Two other arguments, called “–discardphase” and “–dbspl” are also available but will be presented later.</p>
<p>The “signals.dat” file should contain N+1 columns with the time array in column 1 and the signals in arrays 2 to N+1. Here is an example below for two signals. Other examples are provided in the testCases folder.</p>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="mf">0.000000000000000000e+00</span> <span class="mf">2.000000000000000000e+00</span> <span class="mf">1.498961583701809097e+00</span>
<span class="mf">1.000000000000000048e-04</span> <span class="mf">2.084416722356366769e+00</span> <span class="mf">1.562974878816019375e+00</span>
<span class="mf">2.000000000000000096e-04</span> <span class="mf">2.166409772261493227e+00</span> <span class="mf">1.622309033198335859e+00</span>
<span class="mf">3.000000000000000279e-04</span> <span class="mf">2.243625062862666386e+00</span> <span class="mf">1.675260515233458314e+00</span>
<span class="mf">4.000000000000000192e-04</span> <span class="mf">2.313845680645350456e+00</span> <span class="mf">1.720309044997237358e+00</span>
<span class="mf">5.000000000000000104e-04</span> <span class="mf">2.375055534815230018e+00</span> <span class="mf">1.756161242724270322e+00</span>
<span class="mf">6.000000000000000559e-04</span> <span class="mf">2.425497240897346085e+00</span> <span class="mf">1.781787762788217488e+00</span>
<span class="mf">6.999999999999999929e-04</span> <span class="mf">2.463722576667330522e+00</span> <span class="mf">1.796452847048198187e+00</span>
<span class="mf">8.000000000000000383e-04</span> <span class="mf">2.488634061784096740e+00</span> <span class="mf">1.799735449060760439e+00</span>
<span class="mf">9.000000000000000838e-04</span> <span class="mf">2.499516467339062409e+00</span> <span class="mf">1.791541322664194080e+00</span>
<span class="mf">1.000000000000000021e-03</span> <span class="mf">2.496057350657238771e+00</span> <span class="mf">1.772105727862145041e+00</span>
<span class="mf">1.100000000000000066e-03</span> <span class="mf">2.478356025779415273e+00</span> <span class="mf">1.741986676318420413e+00</span>
<span class="mf">1.200000000000000112e-03</span> <span class="mf">2.446920712075631776e+00</span> <span class="mf">1.702048910390296710e+00</span>
<span class="mf">1.300000000000000157e-03</span> <span class="mf">2.402653942855561109e+00</span> <span class="mf">1.653439075675259717e+00</span>
<span class="mf">1.399999999999999986e-03</span> <span class="mf">2.346826652906402355e+00</span> <span class="mf">1.597552799887458574e+00</span>
<span class="mf">1.500000000000000031e-03</span> <span class="mf">2.281041688926065625e+00</span> <span class="mf">1.535994623255990454e+00</span>
<span class="mf">1.600000000000000077e-03</span> <span class="mf">2.207187790496642155e+00</span> <span class="mf">1.470531930875723869e+00</span>
<span class="mf">1.700000000000000122e-03</span> <span class="mf">2.127385362841690775e+00</span> <span class="mf">1.403044209650169183e+00</span>
<span class="mf">1.800000000000000168e-03</span> <span class="mf">2.043925598275371680e+00</span> <span class="mf">1.335469086700656005e+00</span>
<span class="mf">1.899999999999999996e-03</span> <span class="mf">1.959204694215921361e+00</span> <span class="mf">1.269746698522776551e+00</span>
</pre></div>
</div>
<p>When launching the tool, it will ask whether you want to work on a subset of signals. If you answer yes (y), you can enter the IDs of the signals to extract (0 corresponds to the first signal, 1 to the second, etc.).</p>
<p>The tool provides two types of output:</p>
<ul class="simple">
<li><p>A matplotlib figure with some diagnostic plots.</p></li>
<li><p>A text file called “fourier_psd.dat”, containing the fourier coefficients and the psd amplitudes as a function of the frequency.</p></li>
</ul>
<p>The rest of the document explains how to interpret the different diagnostic plots.</p>
</div>


          </div>
          
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper">
<h1 class="logo"><a href="index.html">Spectral</a></h1>








<h3>Navigation</h3>
<ul class="current">
<li class="toctree-l1 current"><a class="current reference internal" href="#">Introduction</a></li>
<li class="toctree-l1"><a class="reference internal" href="#installation">Installation</a></li>
<li class="toctree-l1"><a class="reference internal" href="#tool-usage">Tool usage</a></li>
<li class="toctree-l1"><a class="reference internal" href="usage_example.html">Interpretation of the output plots</a></li>
</ul>

<div class="relations">
<h3>Related Topics</h3>
<ul>
  <li><a href="index.html">Documentation overview</a><ul>
      <li>Previous: <a href="index.html" title="previous chapter">Welcome to Spectral’s documentation!</a></li>
      <li>Next: <a href="usage_example.html" title="next chapter">Interpretation of the output plots</a></li>
  </ul></li>
</ul>
</div>
<div id="searchbox" style="display: none" role="search">
  <h3 id="searchlabel">Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="search.html" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" />
      <input type="submit" value="Go" />
    </form>
    </div>
</div>
<script>$('#searchbox').show(0);</script>








        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="footer">
      &copy;2020, Team COOP.
      
      |
      Powered by <a href="http://sphinx-doc.org/">Sphinx 2.4.4</a>
      &amp; <a href="https://github.com/bitprophet/alabaster">Alabaster 0.7.12</a>
      
      |
      <a href="_sources/spectral_doc.md.txt"
          rel="nofollow">Page source</a>
    </div>

    

    
  </body>
</html>