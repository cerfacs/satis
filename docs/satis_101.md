# SATIS 101

Since Satis is a signals based tool, the easiest way you can think of a usage is by simply calculating the `FFT` (Fast Fourier Transform) as well as the `PSD` (Power Spectral Density).


## Fast Fourier Transform

A Fast Fourier Transform is an algorithm that computes the Discrete Fourier Transform of a discrete signal thus converting a signal from its original domain to a representation in the frequency domain.

$$\mathrm{X}_k = \sum_{n=0}^{N-1}x_n\mathrm{e}^{-i2\pi kn/N} \quad \quad k = 0, ..., N-1   $$

In order to do so using satis, we can for example take a simple signal:

$$ A \cos(2\pi f t + \phi) $$

Which can be easily translated through python:

```python
import numpy as np

time = np.linspace(0,0.1,1000)
signals = 2*np.cos(2*np.pi*50*time)
```

Here we created a signal of amplitude $A = 2$ with a frequency $f = 50 Hz$ and a time vector that spans from $0$ to $0.1$ seconds in $1000$ points.
Which gives us the following figure: 
```python
import numpy as np
import matplotlib.pyplot as plt

time = np.linspace(0,0.1,1000)
signals = 2*np.cos(2*np.pi*50*time)

plt.plot(time,signals)
plt.title('Signal')
plt.xlabel('Time (in sec)')
plt.ylabel('Amplitude')
plt.show()
```
![signal](images/time_signal.png)

Now, you can easiky compute the FFT by using one of the satis function :

```python 
import numpy as np
import matplotlib.pyplot as plt

from satis.satis_library import vanilla_fft

time = np.linspace(0,0.1,1000)
signals = 2*np.cos(2*np.pi*50*time)

freq, amplitude = vanilla_fft(time,signals)

plt.plot(freq,amplitude)
plt.title('Fast Fourier Transform')
plt.xlabel('Frequency (in Hz)')
plt.ylabel('Amplitude')
plt.show()
```
![fft](images/fft_simple.png)

You can see the peak is located at $f= 50Hz$.

## Power Spectral Density

The Power Spectral Density describes the power present in the signal as a function of the frequency, its usual unit is $W/Hz$.

$$\mathrm{S}_{xx} = \lim_{N \to \infty}\frac{(\Delta t)^2}{T} \Bigg |\sum_{n=-N}^{N}x_n\mathrm{e}^{-i2\pi fn\Delta t} \Bigg|$$

Using the same signal as before for the FFT, we can easlity compute and show the Power Spectral Density using satis:

```python 
import numpy as np
import matplotlib.pyplot as plt

from satis.satis_library import vanilla_fft

time = np.linspace(0,0.1,1000)
signals = 2*np.cos(2*np.pi*50*time)

freq, psd = vanilla_PSD(time,signals)

plt.plot(freq,psd)
plt.title('Power Spectral Density')
plt.xlabel('Frequency (in Hz)')
plt.ylabel('Power (in W/Hz)')
plt.show()
```
![psd](images/psd_simple.png)

The peak is located at the same frequency and the power calculated is around $1000$ $W/Hz$.


As simple as it is, you can also imagine collecting the data from an external sources in order to get your time and signal vectors to do the same computation. Nevertheless you might need first to do some treatments on your signal beforehand.
This will thus send you to the next tutorial : [temporal analysis](temporal_analysis_satis.md)
