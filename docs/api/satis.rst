satis package
=============

.. automodule:: satis
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

satis.avbp2global module
------------------------

.. automodule:: satis.avbp2global
   :members:
   :undoc-members:
   :show-inheritance:

satis.avbp2global\_compute\_ftf module
--------------------------------------

.. automodule:: satis.avbp2global_compute_ftf
   :members:
   :undoc-members:
   :show-inheritance:

satis.avbp2global\_diagnostics module
-------------------------------------

.. automodule:: satis.avbp2global_diagnostics
   :members:
   :undoc-members:
   :show-inheritance:

satis.avbp2global\_read\_inputs module
--------------------------------------

.. automodule:: satis.avbp2global_read_inputs
   :members:
   :undoc-members:
   :show-inheritance:

satis.cerfacs\_plot module
--------------------------

.. automodule:: satis.cerfacs_plot
   :members:
   :undoc-members:
   :show-inheritance:

satis.cli module
----------------

.. automodule:: satis.cli
   :members:
   :undoc-members:
   :show-inheritance:

satis.io module
---------------

.. automodule:: satis.io
   :members:
   :undoc-members:
   :show-inheritance:

satis.satis\_library module
---------------------------

.. automodule:: satis.satis_library
   :members:
   :undoc-members:
   :show-inheritance:

satis.temporal_analysis_tool module
-----------------------------------

.. automodule:: satis.temporal_analysis_tool
   :members:
   :undoc-members:
   :show-inheritance:
