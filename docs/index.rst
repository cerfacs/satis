.. Satis documentation master file, created by
   sphinx-quickstart on Tue Sep 15 16:47:13 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Satis's documentation!
====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   readme_copy
   tuto_temporal_analysis
   api/satis


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
